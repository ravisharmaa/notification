<?php

namespace Brick\Notification\Tests;

use Buckhill\Notification\NotificationEventServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app): array
    {
        return [NotificationEventServiceProvider::class];
    }

    protected function getEnvironmentSetUp($app): void
    {
        $app['config']->set('notification.teams.webhook_url', 'https://randomurl.com');
    }
}
