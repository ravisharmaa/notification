<?php

namespace Brick\Notification\Tests\Feature;

use Brick\Notification\Tests\TestCase;
use Buckhill\Notification\DTO\NotificationMessageDTO;
use Buckhill\Notification\Event\SendExternalNotification;
use Buckhill\Notification\Listener\ExternalNotificationListener;
use Buckhill\Notification\Notifier\NotificationResolver;
use Buckhill\Notification\Notifier\Notifiers;
use Illuminate\Http\Client\Request;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class SendExternalNotificationEventTest extends TestCase
{
    public function test_it_should_send_http_request_to_send_notification()
    {
        Http::fake([
            '*' => Http::response([], 200),
        ]);
        $notificationResolver = new NotificationResolver();
        $listener = new ExternalNotificationListener($notificationResolver);
        $listener->handle(new SendExternalNotification(
            new NotificationMessageDTO(
                uuid: 'some-uuid',
                status: 'completed',
                updatedAt: new \DateTime(),
                notifiers: Notifiers::TEAMS
            )
        ));
        Http::assertSent(function (Request $request) {
            return $request->url() == 'https://randomurl.com';
        });
    }

    public function test_it_should_throw_exception_when_a_notification_cannot_be_sent()
    {
        $this->expectException(RequestException::class);
        Http::fake([
            '*' => Http::response([], 400),
        ]);
        $notificationResolver = new NotificationResolver();
        $listener = new ExternalNotificationListener($notificationResolver);
        $listener->handle(new SendExternalNotification(
            new NotificationMessageDTO(
                uuid: 'some-uuid',
                status: 'completed',
                updatedAt: new \DateTime(),
                notifiers: Notifiers::TEAMS
            )
        ));
        Http::assertSent(function (Request $request) {
            return $request->url() == 'https://randomurl.com';
        });
    }
}
