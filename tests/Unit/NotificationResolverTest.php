<?php

namespace Brick\Notification\Tests\Unit;

use Buckhill\Notification\Exception\NotImplemented;
use Buckhill\Notification\Notifier\NotificationResolver;
use Buckhill\Notification\Notifier\NotificationStrategy;
use Buckhill\Notification\Notifier\Notifiers;
use PHPUnit\Framework\TestCase;

class NotificationResolverTest extends TestCase
{
    private NotificationResolver $notificationResolver;

    protected function setUp(): void
    {
        $this->notificationResolver = new NotificationResolver();
    }

    public function test_it_should_resolve_appropriate_notifier()
    {
        $this->assertInstanceOf(NotificationStrategy::class, $this->notificationResolver->resolve(Notifiers::TEAMS));
    }

    public function test_it_should_throw_exception_when_a_notifier_is_not_implemented()
    {
        $this->expectException(NotImplemented::class);
        $this->notificationResolver->resolve(Notifiers::SLACK);
    }
}
