<?php

namespace Buckhill\Notification\DTO;

use Buckhill\Notification\Notifier\Notifiers;

class NotificationMessageDTO implements \JsonSerializable
{
    public function __construct(
        private readonly string $uuid,
        private readonly string $status,
        private readonly \DateTime $updatedAt,
        private readonly Notifiers $notifiers
    ) {
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function getNotifier(): Notifiers
    {
        return $this->notifiers;
    }

    public function jsonSerialize(): array
    {
        return [
            'title' => 'A good message',
            'text' => $this->getStatus(),
        ];
    }
}
