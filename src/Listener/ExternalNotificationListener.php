<?php

namespace Buckhill\Notification\Listener;

use Buckhill\Notification\Event\SendExternalNotification;
use Buckhill\Notification\Exception\NotImplemented;
use Buckhill\Notification\Notifier\NotificationResolver;
use Illuminate\Support\Facades\Log;

class ExternalNotificationListener
{
    public function __construct(
        private readonly NotificationResolver $notificationResolver
    ) {
    }

    public function handle(
        SendExternalNotification $event
    ): void {
        $notification = $event->getNotificationMessage();
        try {
            $this->notificationResolver->resolve($notification->getNotifier())->sendNotification($notification);
        } catch (NotImplemented $exception) {
            Log::info('Could not send notification', [
                'notification' => $notification->getNotifier()->name,
                'exception' => get_class($exception),
            ]);
        }
    }
}
