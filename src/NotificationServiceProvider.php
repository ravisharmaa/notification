<?php

namespace Buckhill\Notification;

use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../src/config/notification.php' => config_path('notification.php'),
        ], 'notification');
    }

    public function register(): void
    {
        $this->app->register(NotificationEventServiceProvider::class);
    }
}
