<?php

namespace Buckhill\Notification\Notifier;

use Buckhill\Notification\Exception\NotImplemented;

class NotificationResolver
{
    public function resolve(Notifiers $notifiers): NotificationStrategy
    {
        return match ($notifiers) {
            Notifiers::TEAMS => app(TeamsNotificationStrategy::class),
            Notifiers::SLACK , Notifiers::DISCORD => throw new NotImplemented('To be implemented'),
        };
    }
}
