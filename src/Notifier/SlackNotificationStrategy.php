<?php

namespace Buckhill\Notification\Notifier;

use Buckhill\Notification\DTO\NotificationMessageDTO;
use Illuminate\Support\Facades\Log;

class SlackNotificationStrategy implements NotificationStrategy
{
    public function sendNotification(NotificationMessageDTO $notificationMessageDTO): void
    {
        Log::info('Sending slack notification is beautiful.');
    }
}
