<?php

namespace Buckhill\Notification\Notifier;

enum Notifiers
{
    case TEAMS;
    case SLACK;
    case DISCORD;
}
