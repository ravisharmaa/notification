<?php

namespace Buckhill\Notification\Notifier;

use Buckhill\Notification\DTO\NotificationMessageDTO;

interface NotificationStrategy
{
    public function sendNotification(NotificationMessageDTO $notificationMessageDTO): void;
}
