<?php

namespace Buckhill\Notification\Notifier;

use Buckhill\Notification\DTO\NotificationMessageDTO;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TeamsNotificationStrategy implements NotificationStrategy
{
    public function sendNotification(NotificationMessageDTO $notificationMessageDTO): void
    {
        try {
            Http::post(config('notification.teams.webhook_url'), $notificationMessageDTO->jsonSerialize())
                ->throwUnlessStatus(200);
        } catch (RequestException $exception) {
            Log::info('Could not send message to remote service', [
                'exception' => get_class($exception),
                'message' => $exception->getMessage(),
            ]);
        }
    }
}
