<?php

namespace Buckhill\Notification\Event;

use Buckhill\Notification\DTO\NotificationMessageDTO;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendExternalNotification implements ShouldQueue
{
    use Dispatchable, SerializesModels;

    public function __construct(
        private readonly NotificationMessageDTO $notificationMessageDTO
    ) {

    }

    public function getNotificationMessage(): NotificationMessageDTO
    {
        return $this->notificationMessageDTO;
    }
}
