<?php

namespace Buckhill\Notification;

use Buckhill\Notification\Event\SendExternalNotification;
use Buckhill\Notification\Listener\ExternalNotificationListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;

class NotificationEventServiceProvider extends EventServiceProvider
{
    protected $listen = [
        SendExternalNotification::class => [
            ExternalNotificationListener::class,
        ],
    ];

    public function boot()
    {
        parent::boot();
    }
}
