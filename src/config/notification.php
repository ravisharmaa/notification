<?php

return [
    'teams' => [
        'webhook_url' => env('TEAMS_WEBHOOK_URL', ''),
    ],

    'slack' => [
        'url' => env('SLACK_URL', ''),
        'channel' => env('SLACK_CHANNEL', ''),
    ],
];
